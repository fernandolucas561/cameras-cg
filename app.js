import { TrackballControls } from 'https://threejs.org/examples/jsm/controls/TrackballControls.js';

const { innerWidth, innerHeight } = window;
const aspectRatio = innerWidth / innerHeight;
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(45, aspectRatio, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();

renderer.setSize(innerWidth, innerHeight);
document.querySelector('body').append(renderer.domElement);

const AxesHelper = new THREE.AxesHelper(5);
scene.add(AxesHelper);

const geometry = new THREE.CylinderGeometry();

const material = new THREE.MeshBasicMaterial({
  color: '#7b0ed4',
  wireframe: true,
  wireframeLinewidth: 2
});

const cylinder = new THREE.Mesh(geometry, material);
scene.add(cylinder);

camera.position.z = 10;
camera.position.x = 1;
camera.position.y = 1;

const controls = new TrackballControls(camera, renderer.domElement);

controls.rotateSpeed = 10;
controls.zoomSpeed = 10;
controls.panSpeed = 10;

const animate = () => {
  requestAnimationFrame(animate);
  controls.update();

  cylinder.rotation.x += 0.1;

  renderer.render(scene, camera);
};

animate();